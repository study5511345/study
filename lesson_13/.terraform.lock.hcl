# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/kreuzwerker/docker" {
  version     = "3.0.2"
  constraints = "3.0.2"
  hashes = [
    "h1:cT2ccWOtlfKYBUE60/v2/4Q6Stk1KYTNnhxSck+VPlU=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.104.0"
  hashes = [
    "h1:hmjNzdigpBU/Q5Lf1eYxUX3VRVkZs7J+rYlDeK9edeI=",
  ]
}
