terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-b"
}

resource "yandex_compute_instance" "vm-1" {
  name = "terraform1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd80bm0rh4rkepi5ksdi"
    }
  }

  network_interface {
    subnet_id = "e2lt2b0n9hhm1qnhj08p"
    nat       = true
  }

  metadata = {
    user-data = "${file("/study/lesson_14/tf1/meta.txt")}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt-get install docker.io curl git -y && sudo curl -SL https://github.com/docker/compose/releases/download/v2.3.3/docker-compose-linux-x86_64 -o /usr/bin/docker-compose && sudo chmod +x /usr/bin/docker-compose  && sudo git clone https://gitlab.com/study5511345/study.git /study && sudo docker-compose -f /study/lesson_11/docker-compose.yml up -d"]

    connection {
      type        = "ssh"
      host        = self.network_interface.0.ip_address
      user        = "alexandrov"
      private_key = "${file("/study/lesson_14/tf1/id_ed25519")}"
    }
  }

}


output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

